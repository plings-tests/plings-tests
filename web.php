<?php
/*
 *  Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
 *  Released as free software under the MIT license,
 *  see the LICENSE file for details.
 */

require_once('web_base.php');

$test = &new GroupTest('Live Website');
$test->addTestCase(new PlingsWWWTest(true));
$test->run(new HtmlReporter());

$test = &new GroupTest('Dev Website');
$test->addTestCase(new PlingsWWWTest(false));
$test->run(new HtmlReporter());

$test = &new GroupTest('Live Mobile Website');
$test->addTestCase(new PlingsMobileTest(true));
$test->run(new HtmlReporter());

$test = &new GroupTest('Dev Mobile Website');
$test->addTestCase(new PlingsMobileTest(false));
$test->run(new HtmlReporter());


?>
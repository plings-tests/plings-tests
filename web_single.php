<?php
/*
 *  Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
 *  Released as free software under the MIT license,
 *  see the LICENSE file for details.
 */

require_once(dirname(__FILE__) . '/simpletest/simpletest.php');
require_once(dirname(__FILE__) . '/simpletest/unit_tester.php');
require_once("web_base.php");

class PlingsSingleTest extends PlingsWWWTest {
    function PlingsSingleTest($aid, $live=true) {
        PlingsWWWTest::PlingsWWWTest($live);
        $this->aid = $aid;
    }
    
    function test_single_web() {
        $this->test_data(true, $this->aid);
    }
    
    function test_single_mobile() {
        $this->test_data(false, $this->aid);
    }
    
    function  isTest  (string $method)  {
        if (strtolower(substr($method, 0, 4)) == "test_single") {
            return ! SimpleTestCompatibility::isA($this, strtolower($method));
        }
        return false;
    }
}

$test = &new GroupTest('Single Activity');
$test->addTestCase(new PlingsSingleTest($_REQUEST["aid"], $_REQUEST["live"]));
$test->run(new HtmlReporter());

?>
<?php
/*
 *  Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
 *  Released as free software under the MIT license,
 *  see the LICENSE file for details.
 */

require_once(dirname(__FILE__) . '/simpletest/xml.php');
require_once(dirname(__FILE__) . '/simpletest/http.php');
require_once("config.php");

class XMLTester extends UnitTestCase {
    function XMLTester($live = true) {
        $apikey = Config::apikey();
        if ($live) {
            $this->xmlurl = "http://feeds.plings.net/xml.activity.php/6/?APIKey=".$apikey."-L&MaxResults=1";
        }
        else {
            $this->xmlurl = "http://feeds.plings.net/xml.activity.php/6/?APIKey=".$apikey."-D&MaxResults=1";
        }
        $this->xml = simplexml_load_file($this->xmlurl);
    }
    
    function test_badapikey() {
        foreach (array("", "APIKey=", "APIKey=ABC123") as $args) {
            $failxml = simplexml_load_file("http://feeds.plings.net/xml.activity.php/0/?".$args);
            $this->assertTrue(isset($failxml->result));
            $this->assertEqual(isset($failxml->result->ErrorCount), "1");
            $this->assertTrue(isset($failxml->result->Errors));
            $this->assertEqual(isset($failxml->result->Errors->InvalidAPIKey), "1");
        }
    }
    
    function check_tags($required, $semirequired, $parent) {
        foreach ($required as $e) {
            $this->assertTrue(isset($parent->{$e}), "Tag missing ".$e);
            $this->assertTrue(strlen((string)$parent->{$e}) > 0, "Tag content missing ".$e);
        }
        foreach ($semirequired as $e) {
            $this->assertTrue(isset($parent->{$e}), "Tag missing ".$e);
        }
        $all = array_merge($required, $semirequired);
        // Currently doesn't check for unexpected tags that are direct chlidren of plings
        foreach ($parent as $selement) {
            $name = $selement->getName();
            $this->assertTrue(in_array($name, $all), "Unexpected tag: ".$name);
        }
    }

    function test_generated() {
        $required = array("year", "month", "monthName", "dayofmonth", "hour", "min", "sec");
        $semirequired = array("requested");
        $this->check_tags($required, $semirequired, $this->xml->generated[0]);
    }
    
    function test_queryDetails() {
        $required = array("Results", "TotalResults", "queryTime");
        $semirequired = array("latitude", "longitude");
        $this->check_tags($required, $semirequired, $this->xml->queryDetails[0]);
    }
    
    function test_activities() {
        $this->assertTrue(isset($this->xml->activities));
        $this->assertTrue(isset($this->xml->activities->activity));
        $required = array("Name", "Starts", "Ends", "Details", "ContactName");
        $semirequired = array("MinAge", "MaxAge", "Cost", "ProviderProjectDept", "ContactNumber", "ContactEmail", "ContactAddress", "categories", "keywords", "ecmareas", "venue", "provider");#
        $this->check_tags($required, $semirequired, $this->xml->activities->activity[0]);
    }
    
    function test_venues() {
        $this->assertTrue(isset($this->xml->activities->activity[0]->venue));
        $required = array("Name", "BuildingNameNo", "Postcode", "Telephone", "LA", "Ward", "Longitude", "Latitude", "LAName", "WardName");
        $semirequired = array("Street", "Town", "PostTown", "County", "PlingsPlacesLink");
        $this->check_tags($required, $semirequired, $this->xml->activities->activity[0]->venue[0]);
    }
    
    function test_provider() {
        $this->assertTrue(isset($this->xml->activities->activity[0]->provider));
        $required = array("Name");
        $semirequired = array("Description", "Website", "Contact", "Email", "Phone", "Fax", "BuildingNameNo", "Street", "Town", "PostTown", "County", "Postcode", "LA", "LAName", "Ward", "WardName", "Latitude", "Longitude", "network");
        $this->check_tags($required, $semirequired, $this->xml->activities->activity[0]->provider[0]);
    }
    
    function test_network() {
        $this->assertTrue(isset($this->xml->activities->activity[0]->provider[0]->network));
        $required = array("name");
        $semirequired = array("description", "website");
        $this->check_tags($required, $semirequired, $this->xml->activities->activity[0]->provider[0]->network[0]);        
    }
        
        /*$browser = &new SimpleBrowser();
        $browser->addHeader('User-Agent: SimpleTest ' . SimpleTest::getVersion());
        $this->assertTrue($browser->get($this->samples() . 'network_confirm.php'));
        $this->assertPattern('/target for the SimpleTest/', $browser->getContent());
        $this->assertPattern('/Request method.*?<dd>GET<\/dd>/', $browser->getContent());
        $this->assertEqual($browser->getTitle(), 'Simple test target file');
        $this->assertEqual($browser->getResponseCode(), 200);
        $this->assertEqual($browser->getMimeType(), 'text/html');*/
}

?>
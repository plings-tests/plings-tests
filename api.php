<?php
/*
 *  Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
 *  Released as free software under the MIT license,
 *  see the LICENSE file for details.
 */

require_once(dirname(__FILE__) . '/simpletest/unit_tester.php');
require_once(dirname(__FILE__) . '/xml.php');

$test = &new GroupTest('Live XML');
$test->addTestCase(new XMLTester(true));
$test->run(new HtmlReporter());

$test = &new GroupTest('Dev XML');
$test->addTestCase(new XMLTester(false));
$test->run(new HtmlReporter());



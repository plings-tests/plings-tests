<?php
/*
 *  Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
 *  Released as free software under the MIT license,
 *  see the LICENSE file for details.
 */

/*
 * Issues - Appears to break when activity/venue name is the same http://wwwdev.plings.net/a/436777
 *
 *
 * Broken? http://www.plings.net/a/578548
 *
 */

require_once(dirname(__FILE__) . '/simpletest/web_tester.php');
require_once(dirname(__FILE__) . '/simpletest/unit_tester.php');
require_once(dirname(__FILE__) . '/simpletest/browser.php');
require_once('config.php');

class PlingsWebTest extends WebTestCase {
    function PlingsWebTest() {
        WebTestCase::WebTestCase();
    }
    
    function test_site() {
        $this->get($this->baseurl."/search");
        $this->clickLink("more info");
        $this->assertTitle(new PatternExpectation('/Activity/'));
        $wantedelements = array("Date", "Start", "End", "Ages", "Cost", "Description", "Organised by", "Venue", "Address", "Telephone", "Stay Safe");
        foreach ($wantedelements as $e) {
            $this->assertText($e);
        }
    }
    
    function test_data($desktop = true, $aid = null) {
        $apikey = Config::apikey();
        if (!$aid) {
            $this->get($this->baseurl."/search");
            $this->clickLink("more info");
            $aid = str_replace($this->baseurl."/a/", "", $this->getUrl());
        }
        else {
            $this->get($this->baseurl."/a/".$aid);
        }
        echo "ActivityID: ".$aid."<br />";
        $xml = simplexml_load_file("http://feeds.plings.net/activity.php/".$aid.".xml?APIKey=".$apikey.$this->flag);
        $this->assertTitle(new PatternExpectation("/".preg_quote((string)$xml->activity->Name, "/")."/"));
        $wantedelements = array(
                $xml->activity->Name,
                $xml->activity->Details,
                $xml->activity->ContactName,
                $xml->activity->ContactNumber,
                $xml->activity->provider->Name,
                $xml->activity->venue->Name,
                $xml->activity->venue->LA->LAName,
                $xml->activity->venue->Ward->WardName,
                $xml->activity->provider->network->name
            );
        foreach ($wantedelements as $e) {
            if ((string)$e) {
                if ($e->getName() == "name") {
                    $bits = explode("-", (string)$e);
                    foreach ($bits as $bit) {
                        $this->assertText(trim($bit), "network not on Activity page");
                    }
                }
                else {
                    $this->assertText(preg_replace("/ +/", " ", strip_tags((string)$e)), $e->getName()." not on Activity page");
                }
            }
        }
        
        $this->clickLink((string)$xml->activity->venue->Name);
        $vid = str_replace($this->baseurl."/v/", "", $this->getUrl());
        $wantedelements = array(
                $xml->activity->venue->Name,
                $xml->activity->venue->LA->LAName,
                $xml->activity->venue->Ward->WardName,
                $xml->activity->venue->BuildingNameNo,
                $xml->activity->venue->Street,
                $xml->activity->venue->Postcode,
                $xml->activity->venue->Telephone,
                $xml->activity->venue->Latitude,
                $xml->activity->venue->Longitude
            );
        foreach ($wantedelements as $e) {
            if ((string)$e) {
                $this->assertText(trim(strip_tags((string)$e)), $e->getName()." not on Venue page");
            }
        }
        if ($desktop) {
            $this->assertPattern("/<input[^>]*plings:place=".$vid."/");
        }
        
        $this->get($this->baseurl."/a/".$aid);
        $this->clickLink((string)$xml->activity->provider->Name);
        $pid = str_replace($this->baseurl."/o/", "", $this->getUrl());
        $wantedelements = array(
                $xml->activity->provider->Name,
                $xml->activity->provider->Contact,
                $xml->activity->provider->Phone,
                $xml->activity->provider->Email,
                $xml->activity->provider->LAName,
                $xml->activity->provider->WardName
            );
        foreach ($wantedelements as $e) {
            if ((string)$e) {
                $this->assertText(trim(strip_tags((string)$e)), $e->getName()." not on Organisation page");
            }
        }
        
    }
}

class PlingsWWWTest extends PlingsWebTest {
    function PlingsWWWTest ($live = true) {
        PlingsWebTest::PlingsWebTest();
        if ($live) {
            $this->baseurl = "http://www.plings.net";
            $this->flag = "-L";
        }
        else {
            $this->baseurl = "http://wwwdev.plings.net";
            $this->flag = "-D";
        }
    }
    
    function check_elements($aid) {
        $apikey = Config::apikey();
        $this->get($this->baseurl . "/a/" . $aid);
        
        // Image
        $b = $this->getBrowser();
        $content = $b->getContent();
        $found = preg_match('/<div id="activity_image"((?!<div).)*(<div.*?>.*?<\/div>((?!<div).)*)<\/div>/', str_replace("\n", " ", $content), $m);
        $this->assertTrue($found, "Image container");
        $imgdiv = $m[0];
        $this->assertTrue(preg_match('/<img src="([^"]*)"/', $imgdiv, $m), $aid . "Image");
        $imgurl = $m[1];
        $this->assertTrue($this->get($imgurl), $aid." Image location");
        preg_match_all('/<a href="([^"]*)"/', $imgdiv, $m);
        $urls = $m[1];
        $this->assertEqual(sizeof($urls), 3, $aid . "Image Attributeion");
        
        // Map
        $this->assertTrue(preg_match('/<object data="([^"]*)".*?<\/object>/', str_replace("\n", " ", $content), $m), "Map");
        $this->assertTrue($this->get($m[1]), "Map location");
    }
    
    function test_elements() {
        $this->check_elements("412963");
    }
}

class PlingsMobileTest extends PlingsWebTest {
    function test_site() {
        $this->get($this->baseurl."/search");
        $this->clickLink("more info");
        # No "Description" or "Venue" text on mobile site
        $wantedelements = array("Date", "Start", "End", "Ages", "Cost", "Organised by","Local Authority", "Address", "Telephone", "Stay Safe");
        foreach ($wantedelements as $e) {
            $this->assertText($e);
        }
    }
    
    function test_data($desktop=false) {
        parent::test_data($desktop);
    }
    
    function test_redirect() {
        $browser = &new SimpleBrowser();
        $browser->addHeader("User-Agent: Mozilla/5.0 (Linux; U; Android 2.1-update1; en-gb; HTC Legend 1.32.161.4 Build/ERE27) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
        $browser->get($this->desktop);
        $this->assertEqual($browser->getUrl(), $this->baseurl."/", "Mobile redirect");
        
        $browser = &new SimpleBrowser();
        $browser->addHeader("User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2.8) Gecko/20100724 Firefox/3.6.8");
        $browser->get($this->desktop);
        $this->assertEqual($browser->getUrl(), $this->desktop, "Desktop non-redirect");
    }
    
    function PlingsMobileTest($live = true) {
        if ($live) {
            $this->desktop = "http://www.plings.net";
            $this->baseurl = "http://m.plings.net";
            $this->flag = "-L";
        }
        else {
            $this->desktop = "http://wwwdev.plings.net";
            $this->baseurl = "http://mdev.plings.net";
            $this->flag = "-D";
        }
    }
}

?>